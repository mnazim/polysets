#!/usr/bin/env bash

echo "You have screwed everything so bad, so wiping is the only option."

read -p "Do you really want to wipe your Docker networks and volumes? [y/N] " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    docker stop $(docker ps -aq)
    docker rm $(docker ps -aq)
    docker network prune -f
    docker volume rm $(docker volume ls --filter dangling=true -q)
else
    echo "Aborted."
fi
