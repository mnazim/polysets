# Local development with Docker 

## Prerequisites 

Install following on your computer,

- [virtualbox](https://www.virtualbox.org/wiki/Downloads)
- [Docker](https://docs.docker.com/install/)
- [docker-machine](https://docs.docker.com/machine/install-machine/)
- [docker-compose](https://docs.docker.com/compose/install/)

## Basic setup

Create a default docker machine

```
docker-machine create --driver virtualbox default
```

Following command will show environment variable needed to talk to the new docker machine.

```
docker-machine env default
```

Load these into your shell with following command,

```
# For bash/zsh
eval "$(docker-machine env default)"
```

```
# For fish
eval (docker-machine env default)
```

Start the newly created docker machine using following command,

```
docker-machine start default
```

## Database setup

:point_right: `sudo` might be required to run docker-compose commands.

In this step we are going to restore database from a postgres dump file.

### Setup environment variables

Make a copy of `env.example` and save it as `.env`.

```
cd /path/to/polysets-server
cp env.example .env
```

### Start `postgres` container

```
cd /path/to/polysets-server
docker-compose up postgres
```

If you see something like following in the output, postgres container is running.

```
Starting polysets_db ... done
Attaching to polysets_db
polysets_db | LOG:  database system was interrupted; last known up at 2018-05-29 21:21:14 UTC
polysets_db | LOG:  database system was not properly shut down; automatic recovery in progress
polysets_db | LOG:  invalid record length at 0/600357B0: wanted 24, got 0
polysets_db | LOG:  redo is not required
polysets_db | LOG:  MultiXact member wraparound protections are now enabled
polysets_db | LOG:  autovacuum launcher started
polysets_db | LOG:  database system is ready to accept connections
```

Just bringing up the `postgres` container should also create database and credentials to access it.

Defaults are,

```
postgres database name = polysets
postgres username = polysets
postgres password = pass
postgres host = postgres
postgres port = 5432
```

_Database settings may be changed by changing relevant values in `.env` file, however, it's recommended these settings be left unchanged for local development._


Now connect to `postgres` container using `psql` - default command line client for PostgreSQL. 
_`psql` must be installed on your system. Use your systems package manager to install it._

```
psql --host=localhost --username=polysets
Password for user polysets: # default password is 'pass'
```

If you see following on your screen, everything is setup properly.

```
psql (9.6.9, server 9.6.8)
Type "help" for help.

polysets=# 
```


_If you cannot install `psql` on your computer for some reason, [Adminer](https://www.adminer.org/) (a web based multi-db client) container is also included in `docker-compose.yml`._

Kill the `postgres` docker container by pressing `Ctrl-C` in the terminal where it's running, and run following command,

```
docker-compose up postgres adminer
```

Go to `http://localhost:8080`. You should see following screen Adminer login screen

![adminer login](https://www.adminer.org/static/screenshots/auth.png)

Select PostgreSQL in `System` dropdown, and login using default credential provided above.
You should see list of databases. 


### Restore from postgres dump file
With `postgres` container running, run following command,

```
pg_restore -Fc --no-acl --no-owner -U polysets -d polysets -h localhost path/to/postgres.dump.file
```


### Start the whole docker cluster

Kill any docker containers that are running, and run following command to start the docker cluster,

```
docker-compose up 
```

Now,

- Polysets API is available at (http://localhost)
- Adminer is available at (http://localhost:8080)
