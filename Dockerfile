FROM python:3.6.4

RUN mkdir /var/polysets-server
WORKDIR /var/polysets-server
COPY requirements.txt /var/polysets-server/requirements.txt
COPY requirements/* /var/polysets-server/requirements/
RUN pip install --upgrade -r requirements/dev.txt
COPY . /var/polysets-server