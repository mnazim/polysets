## API Description

### JWT Authorization header format

`Authorization: jwt <token>`

### POST `/upload`

Requires a valid JWT token.

Accepts `multipart/form-data`.

Upload a zip archive in field named `file`.

*Does not support any other archive format*


```
# response format
{
    "success": true or file,
    "uploaded": "name-of-uploaded-file.zip"
}
```

### GET `/status`

Requires a valid JWT token

Returns status of upload for currently logged in user.


```
# response format
{
    "upload_status": "pending" | "started" | "complete" | "failed",
    "uploaded_at": <timestamp>
}
```

### GET `/u/<username:string>`

Public, does not require JWT token.

Returns imported pictures of a users, following format.

```
# response format
{
    "total": int,
    "page_count": int,
    "next_page": int | null,
    "prev_page": int | null,
    "total_pages": int,

    "pictures": [
      {
          "id": int,
          "user_id": int,
          "s3url": "https://polysets.s3.amazonaws.com/debug/2931/213949689.jpg",
          "title": "string",
          "description": "string"
          "is_draft": bool,
          "created": timestamp,
          "likes_count": int
      },
      ...
      ...
    ]
}
```

#### Query params

- `drafts=<anything>` - fetch only draft pictures.


- `page_size=<int>` - number of records to fetch, defaults to `10`
- `page=<int>` - page number to fetch, defaults to `1`.

*Invalid values for `page` and/or `page_size` will result in an empty list.*

- `sort=<fieldname>` - `sort=fieldname` will sort on the field in ascending order.
- `sort=<fieldname_desc>` - `sort=fieldname` will sort on the field in descending order.

*Invalid values for `sort` with result in default sort of `sort=created_desc`

### GET `/pictures/<picture_id:int>`

Return a picture record identified by `picture_id`.


```
# Response format
{
    "created": datatime,
    "description": text
    "id": int,
    "is_draft": bool,
    "s3url": url
    "title": string,
    "username": string,
    "likes_count": int
}
```
### DELETE `/pictures/<picture_id:int>`

Requires a valid JWT token.

Returns status

 - 404, if not found.
 - 401, if user is not the owner of the picture.
 - 204, if delete is successful.
 - 500, if for some reason s3 image cannot deleted (connection error to s3 is one possible reason for this). The db record is not delete until the corresponding image can be delete from s3

### POST `/pictures/like`

Requires a valid jwt token.

Accepts an ID of a picture in field `id`

Responds with `201 Created` after like has been created.

UI suggestion: Change the like status in UI immediately upon click, and then fire the XHR request. Turn the UI back only in case of error.


### DELETE `/pictures/unlike`

Requires a valid jwt token.

Accepts an ID of a picture in field `id`

Deletes only existing likes on the picture by the user.

Responds with `204 No Content` after like has been deleted.

UI suggestion: Change the like status in UI immediately upon click, and then fire the XHR request. Turn the UI back only in case of error.

### GET `/pictures/likers`

Accepts an ID of a picture in field `id`

responds with a list of usernames who liked the picture


```
# Response

[
    "username1",
    "username2",
    ...
    ...
]
```

### GET `/pictures/top/liked`

Returns list of pictures sorted on likes count.

#### Query params

- `page_size=<int>` - number of records to fetch, defaults to `10`
- `page=<int>` - page number to fetch, defaults to `1`.


### GET `/u/friends`

Requires a valid jwt token.

Returns a list of valid shoplook usernames.

#### Query params

- `page_size=<int>` - number of records to fetch, defaults to `10`
- `page=<int>` - page number to fetch, defaults to `1`.

```
# Response
{
    "next_page": <int>,
    "page": <int>,
    "page_count": <int>,
    "page_size": <int>,
    "prev_page": <int> | <null>,
    "results": [
        {
            "avatar_key": null, 
            "first_name": "Elizabeth", 
            "last_name": "McLaren", 
            "username": "apricity"
        }, 
        {
            "avatar_key": null, 
            "first_name": "Ashley", 
            "last_name": "Bert", 
            "username": "ashleypetrova"
        }
        ...
        ...
    ]
}

The order is based on how likely a username was friend on polyvore.
For example, those username with who current logged in user has exchanged messages is more likely to a friend. So those usernames appear on top.


```
# Response

[
    "username1",
    "username2",
    ...
    ...
]
```




## Deployment notes

The deployment is a normal python webservice deployment.

### Configuration

Configuration is provided via environment variables.

#### Sample configuration

```
DEBUG=1
HOST=127.0.0.1
PORT=8000
PG_USER=developer
PG_PASSWORD=password
PG_HOST=localhost
PG_PORT=5432
PG_DB=polysets

# JWT_SECRET needs to be same as
# the one used on production server
JWT_SECRET=None
JWT_ALGORITHMS=HS256

REQUEST_MAX_SIZE=999000000  # bytes
REQUEST_TIMEOUT=300

# Absolute path to a direcitory to store uploaded zipfiles
UPLOAD_DIR=/tmp/polysets-uploads

# Absolute path to a direcitory to extract the zipfiles
EXTRACTION_DIR=/tmp/polysets-extract


AWS_ACCESS_KEY=key-here
AWS_SECRET_KEY=secret-key-here
S3_BUCKET=polysets

# Limit the number of "extract-import" that can be run simultaneously
MAX_TASKS_ALLOWED=3

# Limits the number of images to be imported from each zipfile
# These many images will be imported from sets.csv and drafts.csv, each.
MAX_IMAGES_IMPORT=500

SENTRY_DSN=
SENTRY_PARAMS_RELEASE=
SENTRY_PARAMS_ENVIRONMENT=
```

### Web Api

To install

1. Create a database.
3. Provide configuration.
2. Create virtualenv and install requirements with `pip install -r requirements.txt`.


To migrate database

```
cd /path/to/polysets-server/
alembic upgrade head
```

To run the API server, run following command.

```
/path/to/polysets-server/app/server.py
```

Sample nginx and systemd configuration are included in the `etc` direcitory.

#### Auth

This service will use the JWT tokens provided by the production server to
authorize the uploads. For this to work correctly, following are needed,

1) System date and time should be same as the date and time on the production server, or as close as practically possible.
2) JWT secret and algorithms must be set to same values as production server.
3) Users must be added to `users` before anyone can use it. `id`, `username`, and `email` fields must have exact same values as on production.

### Extract/import task runner

To extract the uploaded zip files and import images,
schedule following command with cron to run every few minutes.

```
/path/to/polysets-sets/app/server.py
```

It extracts the uploaded zipfiles, pushes the images to s3,
and creates records of the same in `pictures` table.
