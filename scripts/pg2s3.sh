#!/usr/bin/env bash

if [ $# -lt 4 ]
then
    echo "Error: No arguments specified!"
    echo "Usage: pg2s3.sh DB_NAME DB_USER DB_PASSWORD S3_BUCKET_NAME"
    exit
fi


DB_NAME=$1
DB_USER=$2
DB_PASS=$3

BUCKET_NAME=$4

TIMESTAMP=$(date +%F_%T | tr ':' '-')
TEMP_FILE=$(mktemp /tmp/tmp.XXXXXXXXXX)
S3_FILE="s3://$BUCKET_NAME/$DB_NAME-$TIMESTAMP.pgdump"

PGPASSWORD=${DB_PASS} pg_dump -Fc --no-acl --no-owner  -U $DB_USER -d $DB_NAME -f $TEMP_FILE

s3cmd put $TEMP_FILE $S3_FILE
rm "$TEMP_FILE"
