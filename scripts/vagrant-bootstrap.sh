#!/usr/bin/env bash


# postgresql repository
echo 'deb http://apt.postgresql.org/pub/repos/apt/ xenial-pgdg main' | sudo tee /etc/apt/sources.list.d/pgdg.list
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -

# nginx repository
sudo add-apt-repository -y ppa:nginx/stable

# Install base dependencies
sudo apt update

sudo apt-get install -y \
     build-essential \
     libtiff5-dev \
     libjpeg8-dev \
     zlib1g-dev \
     libfreetype6-dev \
     liblcms2-dev \
     libwebp-dev \
     libharfbuzz-dev \
     libfribidi-dev \
     git-core \
     curl \
     make \
     build-essential \
     libssl-dev \
     zlib1g-dev libbz2-dev \
     libreadline-dev \
     libsqlite3-dev \
     wget \
     llvm \
     libncurses5-dev \
     libncursesw5-dev \
     xz-utils tk-dev \
     apt-transport-https \
     ca-certificates \
     curl \
     software-properties-common \
     tmux


sudo apt-get install -y\
     postgresql-9.6 \
     postgresql-server-dev-all \
     nginx \

Switch to non root user for app setup

PROJECT=polysets
PROJECT_HOME=/var/polysets
PROFILE=$HOME/.profile
PYTHON_VERSION=3.6.5
REQUIREMENTS_FILE=$PROJECT_HOME/requirements/dev.txt
PG_VERSION=9.6
PG_DB=$PROJECT
PG_USER=developer
PG_PASSWORD=pass
PG_HOST=localhost
PG_PORT=54321
PG_CONF="/etc/postgresql/$PG_VERSION/main/postgresql.conf"
PG_HBA="/etc/postgresql/$PG_VERSION/main/pg_hba.conf"
PG_DIR="/var/lib/postgresql/$PG_VERSION/main"
PG_CONF_LISTEN="listen_addresses = '*'"
PG_CONF_CLIENT_ENC="client_encoding = utf-8"

read -r -d '' PG_HBA_CONTENT <<'EOF'
# Always trust local connections
local   all             postgres                                trust

# TYPE  DATABASE        USER            ADDRESS                 METHOD

# "local" is for Unix domain socket connections only
local   all             all                                    trust
# IPv4 local connections:
host    all             all             127.0.0.1/32            trust
# IPv6 local connections:
host    all             all             ::1/128                 trust
# Allow replication connections from localhost, by a user with the
# replication privilege.
#local   replication     postgres                                peer
#host    replication     postgres        127.0.0.1/32            md5
#host    replication     postgres        ::1/128                 md5
EOF

read -r -d '' SQL_CREATE_ROLE <<EOF1
CREATE ROLE $PG_USER WITH PASSWORD '$PG_PASSWORD';
ALTER ROLE $PG_USER WITH SUPERUSER;
ALTER ROLE $PG_USER WITH LOGIN;
EOF1

SQL_CREATE_DB="CREATE DATABASE ${PG_DB} WITH OWNER $PG_USER;"

# Postgresql setup
if sudo grep '$PG_HBA_CONTENT' "$PG_HBA"; then
    echo "pg_hba.conf is configured."
else
    echo "Configuring pg_hba.conf."
    echo -e "$PG_HBA_CONTENT" | sudo tee $PG_HBA
fi

if sudo grep '$PG_CONF_LISTEN' "$PG_CONF"; then
    echo "listen_address is set to '*'"
else
    echo "Setting listen_address to '*'"
    echo -e "$PG_CONF_LISTEN" | sudo tee --append $PG_CONF
fi

if sudo grep "$PG_CONF_CLIENT_ENC" "$PG_CONF"; then
    echo "client_encoding is set to utf8"
else
    echo "Setting client_encoding to utf8"
    echo -e "$PG_CONF_CLIENT_ENC" | sudo tee --append $PG_CONF
fi

sudo systemctl restart postgresql

if psql postgres postgres -tAc "SELECT 1 FROM pg_roles WHERE rolname='$PG_USER'" | grep -q 1; then
   echo -e "Postgresql role $PG_USER exists."
else
   echo -e "Creating postgresql role $PG_USER"
   echo -e "postgres postgres -c \"$SQL_CREATE_ROLE\""
   psql postgres postgres -c "$SQL_CREATE_ROLE"
fi

if psql postgres postgres -tAc "SELECT 1 FROM pg_database WHERE datname='$PG_DB'" | grep -q 1; then
    echo -e "Postgresql database $PG_DB exists."
else
    echo -e "Creating postgresql database $PG_DB"
    echo -e "psql postgres postgres -c \"$SQL_CREATE_DB\""
    psql postgres postgres -c "$SQL_CREATE_DB"
    psql postgres postgres -tAc "SELECT 1 FROM pg_database WHERE datname='$PG_DB'"
fi


# Setup pyenv

if [ ! -d $HOME/.pyenv ]; then
    echo 'export PATH="~/.pyenv/bin:$PATH"' >> $PROFILE
    echo 'eval "$(pyenv init -)"' >> $PROFILE
    echo 'eval "$(pyenv virtualenv-init -)"' >> $PROFILE
    source $PROFILE
    git clone https://github.com/pyenv/pyenv-virtualenv.git $(pyenv root)/plugins/pyenv-virtualenv
    curl -L https://github.com/pyenv/pyenv-installer/raw/master/bin/pyenv-installer | bash
    pyenv install $PYTHON_VERSION
else
    echo "Skipping pyenv setup. Directory already exists."
fi

PYENV_HOME=$(pyenv root)/versions/$PROJECT
PYENV_BIN=$PYENV_HOME/bin
PIP=$PYENV_BIN/pip
PYTHON=$PYENV_BIN/python

# Setup virtualenv

if [ ! -d $PYENV_HOME ]; then
    pyenv virtualenv 3.6.5 $PROJECT
else
    echo "SKipping virtualenv creation. Directory already exists."
fi

# cd into project home
cd $PROJECT_HOME
$PIP install -r requirements/dev.txt



