#!/usr/bin/env python

import asyncio
import csv
import logging
import os
import shutil
import sys
import zipfile
from datetime import datetime

import aiobotocore
import botocore
import psycopg2
import raven
import sqlalchemy as sa
from aiohttp.client_exceptions import ClientConnectionError
from aiopg.sa import create_engine
from aioserver import app
from db import (STATUS_COMPLETE, STATUS_FAILED, STATUS_PENDING,
                STATUS_REIMPORT, STATUS_REIMPORT_COMPLETE,
                STATUS_REIMPORT_FAILED, STATUS_REIMPORT_STUCK, STATUS_STARTED,
                pictures, polysets, users)
from raven_aiohttp import AioHttpTransport

log = logging.getLogger()
log.setLevel(logging.INFO)

async def init_db():
    app.db = await create_engine(app.config.PG_DSN)


async def cleanup_files():
    async with app.db.acquire() as conn:
        stmt = polysets.select().where(
            sa.or_(polysets.c.status == STATUS_COMPLETE,
                   polysets.c.status == STATUS_FAILED,
                   polysets.c.status == STATUS_REIMPORT_COMPLETE,
                   polysets.c.status == STATUS_REIMPORT_STUCK))
        log.warning("Cleaning up.")
        async for row in await conn.execute(stmt):
            upload_dir = os.path.join(app.config.UPLOAD_DIR, str(row.user_id))
            img_dir = os.path.join(app.config.EXTRACTION_DIR, str(row.user_id))
            re_upload_dir = os.path.join(app.config.REIMPORT_DIR, str(row.user_id))
            re_img_dir = os.path.join(app.config.REEXTRACT_DIR, str(row.user_id))

            shutil.rmtree(img_dir, ignore_errors=True)
            shutil.rmtree(upload_dir, ignore_errors=True)
            shutil.rmtree(re_img_dir, ignore_errors=True)
            shutil.rmtree(re_upload_dir, ignore_errors=True)


async def upload_to_s3(s3, file_path, user_id):
    filename = os.path.basename(file_path)
    s3key = f"{user_id}/{filename}"
    print("uploaded to->", s3key)
    return s3key

    if app.config.DEBUG:
        s3key = f"debug/{s3key}"

    try:
        await s3.put_object(
            ACL='public-read',
            Bucket=app.config.S3_BUCKET,
            Key=s3key,
            Body=open(file_path, 'rb').read(),
            CacheControl='max-age=31536000')
    except botocore.exceptions.ClientError:
        pass

    s3path = f"{app.config.S3_BASE_URL}/{s3key}"
    return s3path


def to_datetime(s):
    tf = "%Y-%m-%d %H:%M:%S"
    if s is None:
        return datetime(1980, 1, 1)
    try:
        return datetime.strptime(s, tf)
    except ValueError:
        return datetime(1980, 1, 1)


async def process_records(s3, user_id, records, img_dir, is_draft=False):
    id_field = "set_id"
    if is_draft:
        id_field = "draft_id"

    async with app.db.acquire() as conn:
        for rec in records:
            img_path = os.path.join(img_dir, rec[id_field] + ".jpg")
            if os.path.exists(img_path):
                s3url = await upload_to_s3(s3, img_path, user_id)
                # TODO check for duplicates
                stmt = pictures.insert().values(
                    title=rec.get("set_title", ""),
                    description=rec.get("set_description", "")[:1024],
                    s3url=s3url,
                    user_id=user_id,
                    is_draft=is_draft,
                    created=to_datetime(rec.get("set_created_on", "")),
                    updated=datetime.utcnow(),
                    polyvore_set_id=rec.get(id_field, None))
                try:
                    log.warning(f"inserting {s3url}")
                    await conn.execute(stmt)
                except psycopg2.DataError as e:
                    # Can't do anything if the record can't be inserted
                    continue
                except psycopg2.IntegrityError:
                    # Record already exists
                    dt = to_datetime(rec.get("set_created_on", ""))
                    stmt = sa.update(pictures).where(
                        pictures.c.s3url == s3url).values(created=dt)
                    log.warning(f"updatinr {s3url} to {dt}")
                    res = await conn.execute(stmt)


async def import_images(s3, user_id, base_path):
    sets_dir = os.path.join(base_path, "sets")
    drafts_dir = os.path.join(base_path, "drafts")

    sets_csv = os.path.join(base_path, "sets.csv")
    drafts_csv = os.path.join(base_path, "drafts.csv")

    if os.path.exists(sets_csv):
        set_records = list(csv.DictReader(open(sets_csv, 'r')))
        set_records.reverse()
        await process_records(
            s3, user_id, set_records[:app.config.MAX_IMAGES_IMPORT], sets_dir)

    if os.path.exists(drafts_csv):
        draft_records = list(csv.DictReader(open(drafts_csv, 'r')))
        draft_records.reverse()
        await process_records(
            s3,
            user_id,
            draft_records[:app.config.MAX_IMAGES_IMPORT],
            drafts_dir,
            is_draft=True)

    async with app.db.acquire() as conn:
        stmt = sa.update(polysets).where(polysets.c.user_id == user_id).values(
            status=STATUS_COMPLETE)
        await conn.execute(stmt)


async def allow():
    async with app.db.acquire() as conn:
        stmt = polysets.select().where(polysets.c.status == STATUS_STARTED)
        res = await conn.execute(stmt)
        return res.rowcount < app.config.MAX_TASKS_ALLOWED
    return False


async def extract_zip(file_path, extraction_path):
    with zipfile.ZipFile(file_path) as zf:
        namelist = zf.namelist()  # top level dir
        td = namelist[0]  # top level dir
        to_extract = [
            n for n in namelist
            if n.startswith(f"{td}sets/") or n.startswith(f"{td}drafts/")
            or n == f"{td}sets.csv" or n == f"{td}drafts.csv"
        ]
        for member in to_extract:
            zf.extract(member, extraction_path)
        return td


async def extract_polyset(loop):
    if not await allow():
        print("not allowed")
        return

    async with app.db.acquire() as conn:
        failed = False
        stmt = polysets.select().where(
            polysets.c.status == STATUS_PENDING).limit(1)
        res = await conn.execute(stmt)
        if (res.rowcount != 1):
            # Handle error
            return

        row = await res.fetchone()
        print('row 1->', row)

        stmt = sa.update(polysets).where(polysets.c.id == row.id).values(
            status=STATUS_STARTED)
        res = await conn.execute(stmt)

        file_path = os.path.join(app.config.UPLOAD_DIR, row.zip_path)
        try:
            # replace with extract_zip
            with zipfile.ZipFile(file_path) as zf:
                namelist = zf.namelist()  # top level dir
                td = namelist[0]  # top level dir
                to_extract = [
                    n for n in namelist if n.startswith(f"{td}sets/")
                    or n.startswith(f"{td}drafts/") or n == f"{td}sets.csv"
                    or n == f"{td}drafts.csv"
                ]
                extraction_path = os.path.join(app.config.EXTRACTION_DIR,
                                               str(row.user_id))

                print(to_extract)
                for member in to_extract:
                    zf.extract(member, extraction_path)

        except FileNotFoundError:
            failed = True
        except KeyError:
            failed = True
        except zipfile.BadZipFile:
            failed = True

        if not failed:
            # Push zip file to s3

            session = aiobotocore.get_session(loop=loop)
            async with session.create_client(
                    "s3",
                    region_name=app.config.S3_REGION,
                    aws_access_key_id=app.config.AWS_ACCESS_KEY,
                    aws_secret_access_key=app.config.AWS_SECRET_KEY) as s3:

                await upload_to_s3(s3, file_path, row.user_id)
                base_path = os.path.join(extraction_path, td)
                await import_images(s3, row.user_id, base_path)
        else:
            # set failed status

            stmt = sa.update(polysets).where(polysets.c.user_id == row.user_id
                                             ).values(status=STATUS_FAILED)
            await conn.execute(stmt)


# The plan


async def download_s3_file(s3, key, dest):
    try:
        os.makedirs(dest)
    except OSError:
        pass

    save_path = os.path.join(dest, os.path.basename(key))
    log.warning(f"download key=> {key}")
    log.warning(f"save_path => {save_path}")
    if not os.path.exists(save_path):
        # Don't re-download
        try:
            resp = await s3.get_object(Bucket=app.config.S3_BUCKET, Key=key)
            with open(save_path, 'wb') as zf:
                async with resp["Body"] as stream:
                    zf.write(await stream.read())
        except botocore.exceptions.ClientError:
            # Do nothing here calling code handles file not found.
            log.warning('not found')
            pass


async def download_zipfiles(loop):
    async with app.db.acquire() as conn:
        stmt = polysets.select().where(polysets.c.status == STATUS_REIMPORT)
        res = await conn.execute(stmt)
        log.warning(f'current jobs-->{res.rowcount}')
        if (res.rowcount > 25):
            # Don't start too many jobs all at once.
            return

        stmt = polysets.select().where(
            sa.and_(polysets.c.status == STATUS_COMPLETE,
                    polysets.c.id < 800)).limit(1)
        log.warning(stmt)
        res = await conn.execute(stmt)
        log.warning(f'found ->{res.rowcount}')

        if res.rowcount == 0:
            return

        row = await res.fetchone()
        log.warning(f'row =>{row}')

        session = aiobotocore.get_session(loop=loop)
        async with session.create_client(
                "s3",
                region_name=app.config.S3_REGION,
                aws_access_key_id=app.config.AWS_ACCESS_KEY,
                aws_secret_access_key=app.config.AWS_SECRET_KEY) as s3:
            dest = os.path.join(app.config.REIMPORT_DIR, str(row.user_id))
            key = f"{row.user_id}/{os.path.basename(row.zip_path)}"
            await download_s3_file(s3, key, dest)

        file_path = os.path.join(app.config.REIMPORT_DIR, row.zip_path)
        final_status = STATUS_REIMPORT
        extraction_path = os.path.join(app.config.REEXTRACT_DIR, str(row.user_id))

        try:
            top_dir = await extract_zip(file_path, extraction_path)
            base_path = os.path.join(extraction_path, top_dir)
            log.warning(f'base_path =>{base_path}')
        except FileNotFoundError:
            print('file not found')
            final_status = STATUS_REIMPORT_FAILED
        except KeyError:
            print('key error')
            final_status = STATUS_REIMPORT_FAILED
        except zipfile.BadZipFile:
            print('bad file')
            final_status = STATUS_REIMPORT_FAILED

        stmt = sa.update(polysets).where(polysets.c.id == row.id).values(
            status=final_status)
        await conn.execute(stmt)



async def reimport_images(loop):
    async with app.db.acquire() as conn:
        log.warning(conn)
        stmt = polysets.select().where(
            sa.and_(polysets.c.status == STATUS_REIMPORT)).limit(1)
        res = await conn.execute(stmt)

        log.warning(f"to import => {res.rowcount}")
        if res.rowcount == 0:
            return
        row = await res.fetchone()

        await conn.execute(
            sa.update(polysets).where(polysets.c.id == row.id).values(
                status='re-started'))

        log.warning(f"{row.zip_path}")
        zfp = os.path.join(app.config.REIMPORT_DIR, row.zip_path)

        final_status = STATUS_REIMPORT_COMPLETE
        top_dir = None
        try:
            with zipfile.ZipFile(zfp) as zf:
                top_dir = zf.namelist()[0]
        except FileNotFoundError:
            print('file not found')
            final_status = STATUS_REIMPORT_FAILED
        except KeyError:
            print('key error')
            final_status = STATUS_REIMPORT_FAILED
        except zipfile.BadZipFile:
            print('bad file')
            final_status = STATUS_REIMPORT_FAILED

        if final_status == STATUS_REIMPORT_COMPLETE:
            base_path = os.path.join(app.config.REEXTRACT_DIR,
                                     str(row.user_id), top_dir)
            log.warning(f"base_path => {base_path}")
            sets_dir = os.path.join(base_path, "sets")
            drafts_dir = os.path.join(base_path, "drafts")

            sets_csv = os.path.join(base_path, "sets.csv")
            drafts_csv = os.path.join(base_path, "drafts.csv")
            log.warning(f"sets_csv => {sets_csv}")
            log.warning(f"drafts_csv => {drafts_csv}")

            session = aiobotocore.get_session(loop=loop)
            async with session.create_client(
                    "s3",
                    region_name=app.config.S3_REGION,
                    aws_access_key_id=app.config.AWS_ACCESS_KEY,
                    aws_secret_access_key=app.config.AWS_SECRET_KEY) as s3:
                if os.path.exists(sets_csv):
                    set_records = list(csv.DictReader(open(sets_csv, 'r')))

                    await process_records(
                        s3, row.user_id,
                        set_records[:app.config.MAX_IMAGES_IMPORT], sets_dir)

                if os.path.exists(drafts_csv):
                    draft_records = list(csv.DictReader(open(drafts_csv, 'r')))
                    draft_records.reverse()
                    await process_records(
                        s3,
                        row.user_id,
                        draft_records[:app.config.MAX_IMAGES_IMPORT],
                        drafts_dir,
                        is_draft=True)

        await conn.execute(
            sa.update(polysets).where(polysets.c.id == row.id).values(
                status=final_status))


async def try_reimport(loop):
    return;
    await download_zipfiles(loop)
    await reimport_images(loop)


async def main(loop):
    await init_db()
    #await try_reimport(loop)
    await extract_polyset(loop)
    await cleanup_files()


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.set_debug(app.config.DEBUG)
    loop.run_until_complete(main(loop))
