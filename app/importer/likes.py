#!/usr/bin/env python3
#-*- coding: utf-8 -*-

import asyncio
import csv
import fnmatch
import os
import shutil
from datetime import datetime

import psycopg2
from aioserver import app
from db import *
from importer.utils import download_s3_file, extract_zip_members, to_datetime


async def allowed():
    async with app.db.acquire() as conn:
        stmt = polysets.select().where(sa.and_(
            polysets.c.likes_status == STATUS_PROCESSING,
            polysets.c.updated < datetime.utcnow()))
        res = await conn.execute(stmt)
        print(stmt)

        print("===>",res.rowcount)
        if res.rowcount < 7:
            return True
        else:
            return False


async def import_friends(messages_file, user_id):
    if not os.path.exists(messages_file):
        # faile
        print(f"{message_file} does not exist")
        return

    message_records = csv.DictReader(open(messages_file, 'r'))
    async with app.db.acquire() as conn:
        for rec in message_records:
            print(f"adding {rec['message_sender']}")
            print(f"adding {rec['message_recipient']}")
            try:
                stmt = polyvore_friends.insert().values(
                    user_id=user_id, friend_username=rec["message_sender"])
                await conn.execute(stmt)
            except psycopg2.IntegrityError:
                # User already imported
                print(f"{rec['message_sender']} already exists skipping")

            try:
                stmt = polyvore_friends.insert().values(
                    user_id=user_id, friend_username=rec["message_recipient"])
                await conn.execute(stmt)
            except psycopg2.IntegrityError:
                # User already imported
                print(f"{rec['message_recipient']} already exists skipping")


async def import_likes(likes_file, user_id):
    if not os.path.exists(likes_file):
        # faile
        print(f"{likes_file} does not exist")
        return

    records = csv.DictReader(open(likes_file, 'r'))
    async with app.db.acquire() as conn:
        for rec in records:
            if rec["like_object_type"].strip().lower() == "set":
                set_id = rec.get("like_object_id", None)

                if set_id is None:
                    continue

                pattern = f"https://polvore-sets-production.s3.amazonaws.com/%/{set_id}.jpg"
                print(pattern)
                stmt = pictures.select().where(pictures.c.polyvore_set_id == int(set_id))
                res = await conn.execute(stmt)
                if res.rowcount == 1:
                    row = await res.fetchone()
                    picture_id = row.id
                else:
                    picture_id = None

                print(f"-> {picture_id}")

                stmt = pictures_likes.insert().values(
                    picture_id=picture_id,
                    user_id=user_id,
                    polyvore_set_id=set_id,
                    created=to_datetime(rec.get("created", None)))
                try:
                    print(f"inserting {picture_id}, {user_id}")
                    await conn.execute(stmt)
                except psycopg2.IntegrityError:
                    print("Like already recorded")


async def import_friends_and_likes():
    async with app.db.acquire() as conn:
        stmt = polysets.select().where(
            polysets.c.likes_status == STATUS_STARTED).limit(1)
        res = await conn.execute(stmt)

        if res.rowcount == 0:
            print("noting started yet")
            return

        row = await res.fetchone()

        stmt = sa.update(polysets).where(polysets.c.id == row.id).values(
            likes_status=STATUS_PROCESSING, updated=datetime.utcnow())
        await conn.execute(stmt)

        base_dir = os.path.join(app.config.LIKES_DIR, str(row.user_id))
        messages_csv = None
        likes_csv = None
        for root, dirs, files in os.walk(base_dir):
            for f in files:
                if f.endswith("messages.csv"):
                    messages_csv = os.path.join(root, f)
                elif f.endswith("likes.csv"):
                    likes_csv = os.path.join(root, f)
        print(f'messages_csv = {messages_csv}')
        print(f'likes_csv = {likes_csv}')
        print(f'user_id = {row.user_id}')
        if messages_csv is not None:
            await import_friends(messages_csv, row.user_id)

        if likes_csv is not None:
            await import_likes(likes_csv, row.user_id)

        stmt = sa.update(polysets).where(polysets.c.id == row.id).values(
            likes_status=STATUS_COMPLETE, updated=datetime.utcnow())
        await conn.execute(stmt)


async def get_zipfile(loop):
    failed = False
    final_status = STATUS_STARTED
    start = await allowed()
    print("start =>", start)

    if not start:
        print("not allowed, too many tasks already running")
        return

    async with app.db.acquire() as conn:
        stmt = polysets.select().where(
            sa.and_(polysets.c.status == STATUS_COMPLETE,
                    polysets.c.likes_status == None)).limit(1)
        res = await conn.execute(stmt)
        print(f'rowcout = {res.rowcount}')
        row = await res.fetchone()
        print(row)

        stmt = sa.update(polysets).where(
            polysets.c.user_id == row.user_id).values(
                likes_status=STATUS_PROCESSING, updated=datetime.utcnow())
        await conn.execute(stmt)

        zip_download_dir = os.path.join(app.config.LIKES_DIR, str(row.user_id))
        print(f"zip_download_dir = {zip_download_dir}")

        is_downloaded = await download_s3_file(
            access_key=app.config.AWS_ACCESS_KEY,
            secret_key=app.config.AWS_SECRET_KEY,
            region=app.config.S3_REGION,
            bucket=app.config.S3_BUCKET,
            key=row.zip_path,
            dest=zip_download_dir,
            loop=loop)
        if not is_downloaded:
            # zip could not be downloaded
            failed = True
            print("zip cant be downloaded")

        else:
            csv_dir = await extract_zip_members(
                os.path.join(zip_download_dir, os.path.basename(row.zip_path)),
                ["likes.csv", "messages.csv"], zip_download_dir)
            print(f"csv_dir = {csv_dir}")
            if csv_dir is None:
                failed = True

        if failed:
            final_status = STATUS_FAILED

        stmt = sa.update(polysets).where(
            polysets.c.user_id == row.user_id).values(
                likes_status=final_status, updated=datetime.utcnow())
        await conn.execute(stmt)


async def cleanup_files():
    async with app.db.acquire() as conn:
        stmt = polysets.select().where(
            sa.or_(polysets.c.likes_status == STATUS_COMPLETE,
                   polysets.c.likes_status == STATUS_FAILED))

        async for row in await conn.execute(stmt):
            to_delete = os.path.join(app.config.LIKES_DIR, str(row.user_id))
            print(f"delering -> {to_delete}")
            shutil.rmtree(to_delete, ignore_errors=True)


async def main(loop):
    await init_db(app)
    await cleanup_files()
    await import_friends_and_likes()
    await get_zipfile(loop)


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.set_debug(app.config.DEBUG)
    loop.run_until_complete(main(loop))
