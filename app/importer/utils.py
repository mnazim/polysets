# -*- coding: utf-8 -*-

import os
import zipfile
from datetime import datetime

import aiobotocore
import botocore
from aiohttp.client_exceptions import ClientConnectionError


def to_datetime(s):
    """Convert polyvore timestamp to python datetime"""
    tf = "%Y-%m-%d %H:%M:%S"
    if s is None:
        return datetime(1980, 1, 1)
    try:
        return datetime.strptime(s, tf)
    except ValueError:
        return datetime(1980, 1, 1)


async def download_s3_file(access_key, secret_key, region, bucket, key, dest, loop=None):
    try:
        os.makedirs(dest)
    except OSError:
        # Do nothing if directories exist
        pass

    save_path = os.path.join(dest, os.path.basename(key))
    print(f"downloading key=> {key}")
    print(f"save_path => {save_path}")
    if not os.path.exists(save_path):
        # Don't re-download

        session = aiobotocore.get_session(loop=loop)
        async with session.create_client(
                "s3",
                region_name=region,
                aws_access_key_id=access_key,
                aws_secret_access_key=secret_key) as s3client:
            try:
                resp = await s3client.get_object(Bucket=bucket, Key=key)
                if resp["ResponseMetadata"]["HTTPStatusCode"] == 200:
                    with open(save_path, 'wb') as zf:
                        async with resp["Body"] as stream:
                            zf.write(await stream.read())
                        return True
                else:
                    print("s3 response error")
                    return False
            except botocore.exceptions.ClientError as e:
                # Do nothing here calling code handles file not found.
                print('s3 ClientError = ', e)
                return False
            except ClientConnectionError as e:
                print('client connection error =', e)
                return False
    else:
        print("file already exists")
        return True
    print("other error")
    return False



async def extract_zip_members(zip_path, members, extraction_path):
    failed = False
    allowed_members = ["all", "likes.csv", "messages.csv"]

    # Knockout any unknown members
    members = [m for m in members if m in allowed_members]

    try:
        with zipfile.ZipFile(zip_path) as zf:
            namelist = zf.namelist()  # top level dir
            td = namelist[0]  # top level dir

            if "all" in members:
                to_extract = namelist
            else:
                to_extract = [f"{td}{m}" for m in members]

            for member in to_extract:
                zf.extract(member, extraction_path)

    except FileNotFoundError:
        failed = True
    except KeyError:
        failed = True
    except zipfile.BadZipFile:
        failed = True

    if failed:
        return None
    else:
        return os.path.join(extraction_path, td)
