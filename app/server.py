#!/usr/bin/env python

import asyncio
import os
from datetime import datetime
from functools import wraps
from math import ceil
from urllib.parse import urlparse

import aiobotocore
import aiohttp_cors
import aiopg
import botocore
import jwt
import psycopg2
from aiohttp import web
from aiohttp.client_exceptions import ClientConnectionError
from aiohttp.web import middleware
from config import config
from db import *
from utils import paginate_query

routes = web.RouteTableDef()


def login_required(fn):
    """Decorator that restrict access only for authorized users.
    User is considered authorized if get_jwt_payload 
    returns some value.
    """

    @wraps(fn)
    async def wrapped(*args, **kwargs):
        request = args[-1]
        if not isinstance(request, web.BaseRequest):
            msg = ("Incorrect decorator usage. "
                   "Expecting `def handler(request)` "
                   "or `def handler(self, request)`.")
            raise RuntimeError(msg)

        user = get_jwt_payload(request)
        if user is None:
            raise web.HTTPForbidden(text='Invalid auth token')

        ret = await fn(*args, **kwargs)
        return ret

    return wrapped


def get_jwt_payload(request):
    auth = request.headers.get('Authorization', None)
    if auth is None:
        return None

    token = auth.strip()[4:].strip()
    try:
        token = jwt.decode(token, app.config.JWT_SECRET, algorithms=['HS256'])

        now = datetime.utcnow()
        exp = datetime.fromtimestamp(token["exp"])

        if exp < now:
            return None

        if not all(["user_id" in token, "username" in token, "email" in token
                    ]):
            return None

    except jwt.exceptions.InvalidTokenError as e:
        return None

    # If all conditions met
    return token


@middleware
async def add_user(request, handler):
    """
    Middleware adds users to the db, if they do not already exist.
    It also sets request.user = token payload
    """
    token = get_jwt_payload(request)
    if token is not None and all(
        ["user_id" in token, "email" in token, "username" in token]):

        async with app.db.acquire() as conn:
            stmt = users.select().where(
                sa.and_(users.c.id == token["user_id"],
                        users.c.username == token["username"],
                        users.c.email.ilike(token["email"])))
            res = await conn.execute(stmt)

            if res.rowcount == 0:
                stmt = users.insert().values(
                    id=token["user_id"],
                    username=token["username"],
                    email=token["email"],
                    created=datetime.utcnow(),
                    updated=datetime.utcnow())
                await conn.execute(stmt)
            request.user = token
    return await handler(request)


@login_required
async def upload(request):
    async with app.db.acquire() as conn:
        user = request.user

        stmt = polysets.select().where(polysets.c.user_id == user["user_id"])
        res = await conn.execute(stmt)
        if res.rowcount != 0:
            raise web.HTTPBadRequest(text="An upload already exists for user.")

        reader = await request.multipart()
        field = await reader.next()
        assert field.name == "file"
        size = 0
        relative_path = os.path.join(str(user["user_id"]), field.filename)
        upload_to = os.path.join(app.config.UPLOAD_DIR, str(user["user_id"]))
        upload_path = os.path.join(upload_to, field.filename)

        try:
            os.makedirs(upload_to)
        except OSError:
            # Do nothing, it already exists
            pass

        with open(upload_path, 'wb') as f:
            while True:
                chunk = await field.read_chunk()
                if not chunk:
                    break
                size += len(chunk)
                f.write(chunk)

        filep = {"uploaded": field.filename, "success": False, "size": size}

        result = []
        stmt = polysets.insert().values(
            zip_path=relative_path,
            status="pending",
            user_id=user["user_id"],
            created=datetime.utcnow(),
            updated=datetime.utcnow())
        res = await conn.execute(stmt)
        filep["success"] = res.rowcount == 1

    return web.json_response(filep)


@login_required
async def status(request):
    user = request.user
    async with app.db.acquire() as conn:
        result = []
        stmt = polysets.select().where(polysets.c.user_id == user["user_id"])
        res = await conn.execute(stmt)

        if res.rowcount == 0:
            return web.json_response({
                "upload_status": 'not uploaded',
                "uploaded_at": None
            })

        row = await res.fetchone()
        return web.json_response({
            "upload_status": row.status,
            "uploaded_at": row.created.timestamp()
        })


async def picturelist(request):
    username = request.match_info["username"]
    current_user = None

    if hasattr(request, "user"):
        current_user = request.user

    page_size = int(request.query.get('page_size', 10))
    current_page = int(request.query.get('page', 1))
    next_page = current_page + 1
    prev_page = current_page - 1

    if (current_page == 1):
        offset = 0
    else:
        offset = page_size * current_page - page_size

    sort = request.query.get('sort', 'created').lower()
    direction = None
    order = pictures.c.created.desc()

    if sort.endswith('_desc'):
        sort = sort[:-5]
        direction = 'desc'

    if hasattr(pictures.c, sort):
        if direction == 'desc':
            order = getattr(getattr(pictures.c, sort), direction)()
        else:
            order = getattr(pictures.c, sort)

    drafts = "drafts" in request.query
    async with app.db.acquire() as conn:
        stmt = users.select().where(users.c.username == username)
        res = await conn.execute(stmt)

        if (res.rowcount == 0):
            raise web.HTTPNotFound(text="No such user")

        elif (res.rowcount > 1):
            raise web.HTTPServerError(text='Cannot continue.')

        user = await res.fetchone()

        result = []
        res = await conn.execute(
            sa.select([sa.func.count()]).where(
                sa.and_(pictures.c.user_id == user.id,
                        pictures.c.is_draft == drafts)).select_from(pictures))
        row = await res.fetchone()
        total = row[0]
        total_pages = ceil(total / page_size)
        select_fields = [
            *pictures.c,
            sa.func.count(pictures_likes.c.picture_id),
        ]
        if current_user is not None:
            select_fields.append(
                sa.func.max(
                    sa.case(
                        {
                            current_user["user_id"]: 1
                        },
                        value=pictures_likes.c.user_id,
                        else_=0)).label('liked_by_current_user'))

        stmt = sa.select(select_fields).select_from(
            pictures.outerjoin(pictures_likes)).group_by(pictures.c.id).where(
                sa.and_(pictures.c.user_id == user.id,
                        pictures.c.is_draft == drafts)).group_by(
                            pictures.c.id).order_by(order).limit(
                                page_size).offset(offset)

        async for row in conn.execute(stmt):
            if hasattr(row, 'liked_by_current_user'):
                liked_by_current_user = row.liked_by_current_user >= 1
            else:
                liked_by_current_user = False

            result.append({
                "id":
                row.id,
                "s3url":
                row.s3url,
                "title":
                row.title,
                "is_draft":
                row.is_draft,
                "description":
                row.description,
                "username":
                user.username,
                "created":
                row.created.strftime("%Y-%m-%d %H:%M:%S"),
                "likes_count":
                row.count_1,
                "liked_by_current_user":
                liked_by_current_user
            })

        if (current_page >= total_pages):
            next_page = None

        out = {
            "total": total,
            "page_count": len(result),
            "prev_page": prev_page or None,
            "next_page": next_page,
            "total_pages": total_pages,
            "page": current_page,
            "pictures": result
        }

        return web.json_response(out)


async def picture_detail(request):
    picture_id = request.match_info.get("id", None)
    current_user = None

    if picture_id is None:
        return web.HTTPNotFound("Picture does not exist")

    if hasattr(request, "user"):
        current_user = request.user

    async with app.db.acquire() as conn:
        select_fields = [
            *pictures.c,
            sa.func.count(pictures_likes.c.picture_id)
        ]

        if current_user is not None:
            select_fields.append(
                sa.func.max(
                    sa.case(
                        {
                            current_user["user_id"]: 1
                        },
                        value=pictures_likes.c.user_id,
                        else_=0)).label('liked_by_current_user'))

        stmt = sa.select(select_fields).select_from(
            pictures.outerjoin(pictures_likes)).group_by(
                pictures.c.id).where(pictures.c.id == picture_id)

        res = await conn.execute(stmt)
        if res.rowcount == 0:
            raise web.HTTPNotFound(text="Picture does not exist")

        row = await res.fetchone()

        if hasattr(row, 'liked_by_current_user'):
            liked_by_current_user = row.liked_by_current_user >= 1
        else:
            liked_by_current_user = False

        stmt = users.select().where(users.c.id == row.user_id)
        res = await conn.execute(stmt)
        if res.rowcount == 1:
            user = await res.fetchone()
        else:
            user = None

        return web.json_response({
            "id":
            row.id,
            "s3url":
            row.s3url,
            "title":
            row.title,
            "is_draft":
            row.is_draft,
            "description":
            row.description,
            "username":
            user.username,
            "created":
            row.created.strftime("%Y-%m-%d %H:%M:%S"),
            "likes_count":
            row.count_1,
            'liked_by_current_user':
            liked_by_current_user
        })


@login_required
async def delete_picture(request):
    picture_id = request.match_info.get("id", None)
    if picture_id is None:
        raise web.HTTPNotFound(text="Picture does not exist")

    async with app.db.acquire() as conn:
        res = await conn.execute(
            pictures.select().where(pictures.c.id == picture_id))
        if res.rowcount == 0:
            raise web.HTTPNotFound(text="Picture does not exist")

        row = await res.fetchone()

        if row.user_id != request.user["user_id"]:
            raise web.HTTPUnauthorized(
                text="Picture does not belong to the user.")

        session = aiobotocore.get_session(loop=request.loop)
        async with session.create_client(
                "s3",
                region_name=app.config.S3_REGION,
                aws_access_key_id=app.config.AWS_ACCESS_KEY,
                aws_secret_access_key=app.config.AWS_SECRET_KEY) as s3:
            url = urlparse(row.s3url)
            try:
                resp = await s3.delete_object(
                    Bucket=app.config.S3_BUCKET, Key=url.path)
                if resp["ResponseMetadata"]["HTTPStatusCode"] == 204:
                    stmt = pictures.delete().where(pictures.c.id == picture_id)
                    res = await conn.execute(stmt)
                    raise web.HTTPNoContent(text="Deleted")
            except (botocore.exceptions.ClientError, ClientConnectionError):
                pass

        raise web.HTTPInternalServerError(
            text="Could not delete requested picture.")


@login_required
async def like_picture(request):
    data = await request.post()
    picture_id = data.get("id", None)
    if picture_id is None:
        raise web.HTTPNotFound(text="Picture does not exist")

    async with app.db.acquire() as conn:
        res = await conn.execute(
            pictures.select().where(pictures.c.id == picture_id))

        if res.rowcount == 0:
            raise web.HTTPNotFound(text="Picture does not exist")

        pic = await res.fetchone()
        polyvore_set_id, _ = os.path.splitext(os.path.basename(pic.s3url))
        data = dict(
            picture_id=pic.id,
            user_id=request.user['user_id'],
            polyvore_set_id=polyvore_set_id,
            created=datetime.utcnow())

        res = await conn.execute(pictures_likes.select().where(
            sa.and_(pictures_likes.c.picture_id == picture_id,
                    pictures_likes.c.user_id == request.user["user_id"])))

        if res.rowcount == 0:
            await conn.execute(pictures_likes.insert().values(**data))

        raise web.HTTPCreated(text="liked")


@login_required
async def unlike_picture(request):
    data = await request.post()
    picture_id = data.get("id", None)
    if picture_id is None:
        raise web.HTTPNotFound(text="Picture does not exist")

    async with app.db.acquire() as conn:
        await conn.execute(pictures_likes.delete().where(
            sa.and_(pictures_likes.c.picture_id == picture_id,
                    pictures_likes.c.user_id == request.user["user_id"])))

        raise web.HTTPNoContent(text="unliked")


async def picture_likers(request):

    picture_id = int(request.query.get('id', None))
    if picture_id is None:
        raise web.HTTPNotFound(text="Picture not found")

    async with app.db.acquire() as conn:
        stmt = sa.select([users.c.username]).select_from(
            users.join(pictures_likes,
                       pictures_likes.c.user_id == users.c.id)).where(
                           pictures_likes.c.picture_id == picture_id)

        results = []
        async for row in await conn.execute(stmt):
            results.append(row.username)

        return web.json_response(results)


@login_required
async def polyvore_friends_list(request):
    async with app.db.acquire() as conn:
        stmt = users.select().where(
            users.c.username == request.user["username"])
        res = await conn.execute(stmt)
        if res.rowcount == 0:
            raise web.HTTPNotFound(text="User does not exist")

        row = await res.fetchone()

        result = []
        stmt = polyvore_friends.select().where(
            polyvore_friends.c.user_id == request.user["user_id"])
        async for row in await conn.execute(stmt):
            result.append(row.friend_username)

        stmt = sa.select([users.c.username]).select_from(
            pictures.outerjoin(
                pictures_likes,
                pictures.c.id == pictures_likes.c.picture_id).outerjoin(
                    users, pictures.c.user_id == users.c.id)).where(
                        pictures_likes.c.user_id == request.user[
                            "user_id"]).distinct()

        async for row in await conn.execute(stmt):
            result.append(row.username)

        stmt = sa.select(
            [users.c.username, users.c.first_name,
             users.c.last_name, users.c.avatar_key]).select_from(users).where(
                 sa.and_(
                     users.c.username.in_(result),
                     users.c.username != request.user["username"])).order_by(
                         users.c.username)
        final_result = await paginate_query(
            conn, stmt, query=request.query, as_dict=False)

        return web.json_response(final_result)


async def top_liked_feed(request):
    current_user = None
    if hasattr(request, "user"):
        current_user = request.user

    page_size = int(request.query.get('page_size', 10))
    current_page = int(request.query.get('page', 1))
    next_page = current_page + 1
    prev_page = current_page - 1
    if prev_page == 0:
        prev_page = None

    if (current_page == 1):
        offset = 0
    else:
        offset = page_size * current_page - page_size

    async with app.db.acquire() as conn:
        stmt = sa.select([users.c.username, *pictures.c]).select_from(
            pictures.join(users)).order_by(
                pictures.c.likes_count.desc().nullslast()).limit(
                    page_size).offset(offset)

        result = []
        async for row in conn.execute(stmt):
            created = row.created.strftime("%Y-%m-%d %H:%M:%S")
            result.append({
                "id": row.id,
                "s3url": row.s3url,
                "title": row.title,
                "is_draft": row.is_draft,
                "description": row.description,
                "username": row.username,
                "created": created,
                "likes_count": row.likes_count,
                "liked_by_current_user": False
            })

        out = {
            "page_count": len(result),
            "prev_page": prev_page,
            "next_page": next_page,
            "page": current_page,
            "pictures": result
        }

        return web.json_response(out)


app = web.Application()
app.config = config

app.add_routes(routes)

cors = aiohttp_cors.setup(
    app,
    defaults={
        app.config.CORS_ORIGIN:
        aiohttp_cors.ResourceOptions(
            allow_credentials=True,
            expose_headers="*",
            allow_headers="*",
        )
    })

friends_list_resource = cors.add(app.router.add_resource("/u/friends"))
cors.add(friends_list_resource.add_route("GET", polyvore_friends_list))

status_resource = cors.add(app.router.add_resource("/status"))
cors.add(status_resource.add_route("GET", status))

upload_resource = cors.add(app.router.add_resource("/upload"))
cors.add(upload_resource.add_route("POST", upload))

pic_list_resource = cors.add(app.router.add_resource("/u/{username:[-\w]+}"))
cors.add(pic_list_resource.add_route("GET", picturelist))

like_pic_resource = cors.add(app.router.add_resource("/pictures/like"))
cors.add(like_pic_resource.add_route("POST", like_picture))

unlike_pic_resource = cors.add(app.router.add_resource("/pictures/unlike"))
cors.add(unlike_pic_resource.add_route("POST", unlike_picture))

pic_likers_resource = cors.add(app.router.add_resource("/pictures/likers"))
cors.add(pic_likers_resource.add_route("GET", picture_likers))

pic_detail_resource = cors.add(app.router.add_resource(r"/pictures/{id:\d+}"))
cors.add(pic_detail_resource.add_route("GET", picture_detail))

del_pic_resource = cors.add(app.router.add_resource(r"/pictures/{id:\d+}"))
cors.add(del_pic_resource.add_route("DELETE", delete_picture))

top_liked_pics_resource = cors.add(
    app.router.add_resource("/pictures/top/liked"))
cors.add(top_liked_pics_resource.add_route("GET", top_liked_feed))

app.on_startup.append(init_db)
app.on_cleanup.append(close_db)
app.middlewares.append(add_user)

if __name__ == "__main__":
    web.run_app(app, host='0.0.0.0', port=8000)
