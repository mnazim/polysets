import enum
import sqlalchemy as sa
from aiopg.sa import create_engine

meta = sa.MetaData()

users = sa.Table("users", meta, sa.Column("id", sa.BigInteger, nullable=False),
                 sa.Column(
                     "username", sa.Unicode(256), nullable=False, unique=True),
                 sa.Column(
                     "email", sa.Unicode(256), nullable=False, unique=True),
                 sa.Column("created", sa.DateTime, nullable=False),
                 sa.Column("updated", sa.DateTime, nullable=False),
                 sa.Column("first_name", sa.Unicode(256)),
                 sa.Column("last_name", sa.Unicode(256)),
                 sa.Column("avatar_key", sa.Unicode(512)),
                 sa.PrimaryKeyConstraint("id", name="user_id_pkey"))

STATUS_PENDING = "pending"
# For when a task wants other tasks to hold off a set
STATUS_PROCESSING= "processing" 
STATUS_STARTED = "started"
STATUS_COMPLETE = "complete"
STATUS_FAILED = "failed"
STATUS_REIMPORT = "reimport"
STATUS_REIMPORT_COMPLETE = "re-complete"
STATUS_REIMPORT_FAILED = "re-failed"
STATUS_REIMPORT_STUCK = "re-stuck"

polysets = sa.Table("polysets", meta,
                    sa.Column("id", sa.BigInteger, nullable=False),
                    sa.Column("zip_path", sa.Unicode(512), nullable=False),
                    sa.Column(
                        "status",
                        sa.Unicode(16),
                        server_default=STATUS_PENDING,
                        nullable=False),
                    sa.Column("likes_status", sa.Unicode(16)),
                    sa.Column(
                        "user_id", sa.BigInteger, nullable=False, unique=True),
                    sa.Column("created", sa.DateTime, nullable=False),
                    sa.Column("updated", sa.DateTime, nullable=False),
                    sa.PrimaryKeyConstraint("id", name="polyset_id_pkey"),
                    sa.ForeignKeyConstraint(
                        ["user_id"], [users.c.id],
                        name="polyset_user_id_fkey",
                        ondelete="CASCADE"))

pictures = sa.Table("pictures", meta,
                    sa.Column("id", sa.BigInteger, nullable=False),
                    sa.Column("title", sa.Unicode(512), nullable=False),
                    sa.Column("description", sa.Unicode(1024), nullable=False),
                    sa.Column(
                        "s3url", sa.Unicode(512), nullable=False, unique=True),
                    sa.Column("user_id", sa.BigInteger, nullable=False),
                    sa.Column("is_draft", sa.Boolean, nullable=False),
                    sa.Column("created", sa.DateTime, nullable=False),
                    sa.Column("updated", sa.DateTime, nullable=False),
                    sa.Column("polyvore_set_id", sa.BigInteger),
                    sa.Column("likes_count", sa.Integer, server_default="0"),
                    sa.Column("likes_count_updated", sa.DateTime),
                    sa.PrimaryKeyConstraint("id", name="picture_id_pkey"),
                    sa.ForeignKeyConstraint(
                        ["user_id"], [users.c.id],
                        name="picture_user_id_fkey",
                        ondelete="CASCADE"))

pictures_likes_count_idx = sa.Index(
    "pictures_likes_count_desc_idx",
    pictures.c.likes_count.desc().nullslast())


pictures_likes = sa.Table("pictures_likes", meta,
                          sa.Column("id", sa.BigInteger, nullable=False),
                          sa.Column(
                              "picture_id", sa.BigInteger),
                          sa.Column("user_id", sa.BigInteger, nullable=False),
                          sa.Column("polyvore_set_id", sa.BigInteger, nullable=False),
                          sa.Column("created", sa.DateTime, nullable=False),
                          sa.PrimaryKeyConstraint(
                              "id", name="pictures_likes_id_pkey"),
                          sa.ForeignKeyConstraint(
                              ["picture_id"], [pictures.c.id],
                              name="pictures_likes_picture_id_fkey",
                              ondelete="CASCADE"),
                          sa.ForeignKeyConstraint(
                              ["user_id"], [users.c.id],
                              name="pictures_likes_user_id_fkey",
                              ondelete="CASCADE"))

polyvore_friends = sa.Table(
    "polyvore_friends", meta, sa.Column("id", sa.BigInteger, nullable=False),
    sa.Column("user_id", sa.BigInteger, nullable=False),
    sa.Column("friend_username", sa.Unicode(256), nullable=False),
    sa.PrimaryKeyConstraint("id", name="polyvore_friends_id_pkey"),
    sa.UniqueConstraint(
        "user_id",
        "friend_username",
        name="pictures_likes_user_id_friend_username_uniq"),
    sa.ForeignKeyConstraint(
        ["user_id"], [users.c.id],
        name="pictures_likes_user_id_fkey",
        ondelete="CASCADE"))


async def init_db(app):
    engine = await create_engine(app.config.PG_DSN)
    app.db = engine


async def close_db(app):
    app.db.close()
    await app.db.wait_closed()
