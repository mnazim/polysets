#!/usr/bin/env python

import os
import asyncio
import sqlalchemy as sa
from datetime import datetime, timedelta
from db import init_db, pictures, pictures_likes
from aioserver import app


# Counts file keep a pointer to offset to pictures table,
# and the total number of rows in pictures.
counts_file_path = os.environ.get('LIKES_COUNTS_FILE', 'likes_counts.txt')

def read_counts():
    counts = {
        # offset for next run
        'offset': 0,
        # total number of records in pictures table
        'total': 0,
    }
    try:
        with open(counts_file_path, 'r') as f:
            line = f.readline().strip()
            line = line.split(',')
            if len(line) == 2:
                counts['offset'] = int(line[0])
                counts['total'] = int(line[1])
    except FileNotFoundError:
        # Nothing to do, defaults counts initialized at start.
        pass
    except ValueError:
        # Nothing to do, defaults counts initialized at start.
        pass

    return counts


def update_counts(offset, total):
    if offset >= total:
        # Reset offset back to start as soon as it crosses total.
        offset = 0

    with open(counts_file_path, 'w') as f:
        f.write(f'{offset},{total}')


async def count_likes(picture_id):
    async with app.db.acquire() as conn:
        stmt = sa.select([sa.func.count()]).select_from(pictures_likes).where(
            pictures_likes.c.picture_id == picture_id)
        res = await conn.execute(stmt)
        row = await res.fetchone()
        print(f'counting for => {picture_id}')
        stmt = sa.update(pictures).where(pictures.c.id == picture_id).values(
            likes_count=row.count_1, likes_count_updated=datetime.utcnow())
        await conn.execute(stmt)


async def queue_to_count(limit):
    counts = read_counts()
    print('->', counts)
    offset = counts['offset']
    async with app.db.acquire() as conn:
        td = datetime.utcnow() - timedelta(hours=12)
        stmt  = sa.select([sa.func.count()]).select_from(pictures)
        res = await conn.execute(stmt)
        row = await res.fetchone()
        total_pics = row.count_1
        update_counts(offset + limit, total_pics)

        # NOTE: we are sorting on created ASC intentionally,
        # this helps us in keeping the sort stable, because
        # we need to keep limit/offset window stable, so
        # multiple instances of this task don't queue same
        # pictures over and over.
        stmt = (pictures.select()
                .order_by(pictures.c.created)
                .offset(offset)
                .limit(limit))

        async for row in await conn.execute(stmt):
            await count_likes(row.id)


async def start():
    await init_db(app)
    await queue_to_count(int(os.environ.get('LIKES_COUNTER_QUEUE_LIMIT', 1000)))


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.set_debug(app.config.DEBUG)
    loop.run_until_complete(start())
