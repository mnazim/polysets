#!/usr/bin/env python

import os
from datetime import datetime, timedelta
import sqlsoup
import sqlalchemy as sa

# Counts file keep a pointer to offset to  shoplook_db.accounts_user table,
# and the total number of users in shoplookio_db.accounts_users,
counts_file_path = os.environ.get('USERSYNC_COUNTS_FILE', 'usersync_counts.txt')


def read_counts():
    counts = {
        # offset for next run
        'offset': 0,
        # total number of records in pictures table
        'total': 0,
    }
    try:
        with open(counts_file_path, 'r') as f:
            line = f.readline().strip()
            line = line.split(',')
            if len(line) == 2:
                counts['offset'] = int(line[0])
                counts['total'] = int(line[1])
    except FileNotFoundError:
        # Nothing to do, defaults counts initialized at start.
        pass
    except ValueError:
        # Nothing to do, defaults counts initialized at start.
        pass

    return counts


def update_counts(offset, total):
    if offset >= total:
        # Reset offset back to start as soon as it crosses total.
        offset = 0

    with open(counts_file_path, 'w') as f:
        f.write(f'{offset},{total}')


def update_user(polysets_db, **kwargs):
    # NOTE: This function must never reference shoplookio_db for anything.
    user_id = kwargs.pop('user_id', None)
    username = kwargs.pop('username', None)
    email = kwargs.pop('email', None)
    first_name = kwargs.pop('first_name', None)
    last_name = kwargs.pop('last_name', None)
    avatar_key = kwargs.pop('avatar_key', None)

    if not all([user_id, username, email]):
        print(
            f"Invalid data (user_id={user_id}, username={username}, email={email}, first_name={first_name}, last_name={last_name}). Skipping."
        )
        return

    polysets_user = polysets_db.users.get(user_id)
    if polysets_user is not None:
        if not all([polysets_user.username == username,
                    polysets_user.email == email,
                    polysets_user.avatar_key == avatar_key,
                    polysets_user.first_name == first_name,
                    polysets_user.last_name == last_name]):
            # Update the record on polysets_db
            print(f"=> found {polysets_user.id}")
            print(f"  -> changing {polysets_user.username} -> {username}")
            print(f"  -> changing {polysets_user.email} -> {email}")
            print(f"  -> changing {polysets_user.first_name} -> {first_name}")
            print(f"  -> changing {polysets_user.last_name} -> {last_name}")
            print(f"  -> changing {polysets_user.avatar_key} -> {avatar_key}")
            polysets_user.username = username
            polysets_user.email = email
            polysets_user.first_name = first_name
            polysets_user.last_name = last_name
            polysets_user.last_name = last_name
            polysets_user.avatar_key = avatar_key
            polysets_user.updated = datetime.utcnow()
            print("-" * 80)
    else:
        # Insert the new user record
        tbl = polysets_db.users._table
        count = polysets_db.users.filter(sa.or_(tbl.c.id == user_id,
                                        tbl.c.username == username,
                                        tbl.c.email == email)).count()
        if count == 0:
            # Avoid IntegrityError
            print(f'inserting {user_id}, {username}, {email}')
            polysets_db.users.insert(id=user_id,
                                    username=username,
                                    email=email,
                                    first_name=first_name,
                                    last_name=last_name,
                                    avatar_key=avatar_key,
                                    created=datetime.utcnow(),
                                    updated=datetime.utcnow())
        else:
            print("skipping insert")




def sync_all_users(shoplookio_db, polysets_db):
    counts = read_counts()
    print('-> ', counts)
    offset = counts['offset']
    total_users = len(shoplookio_db.accounts_user.all())
    limit = int(os.environ.get('SYNCUSER_QUEUE_MAX', 10000))
    print(f"total_users = {total_users}")
    update_counts(offset + limit, total_users)
    for shoplook_user in shoplookio_db.accounts_user.order_by('id').limit(limit).offset(offset):
        print(shoplook_user.id)
        update_user(
            polysets_db,
            user_id=shoplook_user.id,
            username=shoplook_user.username,
            email=shoplook_user.email,
            first_name=shoplook_user.first_name,
            last_name=shoplook_user.last_name,
            avatar_key=shoplook_user.avatar_key,
        )

    polysets_db.commit()


def start():
    shoplookio_dburl = os.environ.get(
        'SHOPLOOK_DBURL', "postgres://developer:pass@localhost/shoplookio")
    polysets_dburl = os.environ.get(
        'POLYSETS_DBURL', "postgres://developer:pass@localhost/polysets")
    if not all([shoplookio_dburl, polysets_dburl]):
        raise Exception(
            "SHOPLOOK_DBURL and POLYSETS_DBURL environment variables must be set."
        )
    shoplookio_db = sqlsoup.SQLSoup(shoplookio_dburl)
    polysets_db = sqlsoup.SQLSoup(polysets_dburl)

    started_at = datetime.utcnow()
    sync_all_users(shoplookio_db, polysets_db)
    ended_at = datetime.utcnow()
    td = ended_at - started_at
    print (f"===> took {td.total_seconds()} seconds.")


if __name__ == "__main__":
    start()
