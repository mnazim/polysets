import os


__all__ = ["config"]


class Config(object):
    DEBUG = "DEBUG" in os.environ
    HOST = os.environ.get("HOST", "127.0.0.1")
    PORT = os.environ.get("POST", 8000)
    SANIC_WORKERS = os.environ.get("SANIC_WORKERS", 3)
    PG_USER = os.environ.get("PG_USER", "developer")
    PG_PASSWORD = os.environ.get("PG_PASSWORD", "password")
    PG_HOST = os.environ.get("PG_HOST", "localhost")
    PG_PORT = os.environ.get("PG_PORT", 5432)
    PG_DB = os.environ.get("PG_DB", "polysets")
    JWT_SECRET = os.environ.get("JWT_SECRET", "None")
    JWT_ALGORITHMS = ["HS256"]
    REQUEST_MAX_SIZE = 999000000  # bytes
    REQUEST_TIMEOUT = 300
    UPLOAD_DIR = os.path.abspath(os.environ.get("UPLOAD_DIR", "./uploads/"))
    EXTRACTION_DIR = os.path.abspath(os.environ.get("EXTRACTION_DIR", "./extract/"))
    REIMPORT_DIR = os.path.abspath(os.environ.get("REIMPORT_DIR", "./reimport/"))
    REEXTRACT_DIR = os.path.abspath(os.environ.get("REEXTRACT_DIR", "./reextract/"))
    AWS_ACCESS_KEY = os.environ.get("AWS_ACCESS_KEY", "AKIAJ5NCRIJUET2HAKSQ")
    AWS_SECRET_KEY = os.environ.get("AWS_SECRET_KEY",
                                    "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")
    S3_REGION  = os.environ.get("S3_REGION", 'us-west-1')
    S3_BUCKET = os.environ.get("S3_BUCKET", "polysets")
    MAX_TASKS_ALLOWED = int(os.environ.get("MAX_TASKS_ALLOWED", 3))
    MAX_IMAGES_IMPORT = int(os.environ.get("MAX_IMAGES_IMPORT", 500))
    SENTRY_DSN = os.environ.get("SENTRY_DSN", "https://7781c2d86c7a497f8c3de6bc1684d8b8:6b89dee9ed7946aa8ccbc4445e916209@sentry.io/278647")
    SENTRY_PARAMS = {
        "release": os.environ.get("SENTRY_PARAMS_RELEASE", '0.1'),
        "environment": os.environ.get("SENTRY_PARAMS_ENVIRONMENT", 'debug')
    }
    CORS_ORIGIN=os.environ.get("CORS_ORIGIN", '*')
    LIKES_DIR = os.environ.get("LIKES_DIR", './likes')

    @property
    def S3_BASE_URL(self):
        return f"https://{self.S3_BUCKET}.s3.amazonaws.com"

    @property
    def PG_DSN(self):
        return (f"postgres://{self.PG_USER}"
                f":{self.PG_PASSWORD}"
                f"@{self.PG_HOST}"
                f":{self.PG_PORT}/{self.PG_DB}")


config = Config()
