async def paginate_query(conn, stmt, **kwargs):
    results_key = kwargs.pop("results_key", "results")
    out = {
        "page": 1,
        "page_size": 10,
    }
    out.update(kwargs.pop("query", {}))
    out["page_size"] = int(out["page_size"])
    out["page"] = int(out["page"])
    out["next_page"] = out["page"] + 1
    out["prev_page"] = out["page"] - 1

    if out["prev_page"] == 0:
        out["prev_page"] = None

    if (out["page"] == 1):
        offset = 0
    else:
        offset = out["page_size"] * out["page"] - out["page_size"]

    final_stmt = stmt.offset(offset).limit(out["page_size"])

    results = []
    async for row in await conn.execute(final_stmt):
        results.append({k: row[k] for k in row})

    out[results_key] = results
    out["page_count"] = len(results)
    return out

